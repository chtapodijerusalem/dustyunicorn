#ifndef DUSTY_UNICORN_DCOW_H
#define DUSTY_UNICORN_DCOW_H

#include <sys/types.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <pthread.h>
#include <unistd.h>
#include <sys/stat.h>
#include <string.h>
#include <stdint.h>

#ifdef DEBUG
#include <android/log.h>
#define LOGI(...) __android_log_print(ANDROID_LOG_INFO, "DIRTY C0W", __VA_ARGS__)
#define LOGV(...) __android_log_print(ANDROID_LOG_VERBOSE, "DIRTY C0W", __VA_ARGS__)
#define LOGW(...) __android_log_print(ANDROID_LOG_WARN, "DIRTY C0W", __VA_ARGS__)
#define LOGE(...) __android_log_print(ANDROID_LOG_ERROR, "DIRTY C0W", __VA_ARGS__)
#else
#define LOGI(...) ;
#define LOGV(...) ;
#define LOGW(...) ;
#define LOGE(...) ;
#endif


struct mem_arg  {
    void *offset;
    void *patch;
    off_t patch_size;
    const char *fname;
    volatile int stop;
    volatile int success;
};

int dirty_c0w(char* filename, char* newFile);

//int dc0w(char* filename, mem_arg* ma);

#endif //DUSTY_UNICORN_DCOW_H
