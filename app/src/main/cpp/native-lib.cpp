#include <jni.h>
#include <string>
#include <pwd.h>
#include <unistd.h>
#include <pthread.h>

#include <iostream>
#include <stdexcept>
#include <stdio.h>
#include <string>


#ifndef DEBUG
#define DEBUG
#endif

#include "dcow.h"

std::string exec(const char* cmd) {
    char buffer[128];
    std::string result = "";
    FILE* pipe = popen(cmd, "r");
    if (!pipe) throw std::runtime_error("popen() failed!");
    try {
        while (!feof(pipe)) {
            if (fgets(buffer, 128, pipe) != NULL)
                result += buffer;
        }
    } catch (...) {
        pclose(pipe);
        throw;
    }
    pclose(pipe);
    return result;
}


extern "C"
JNIEXPORT jstring JNICALL
Java_ru_kaf42_dustyunicorn_MainActivity_stringFromJNI(
        JNIEnv* env,
        jobject /* this */) {
    uid_t uid = getuid();
    LOGI("UserIDs", "Runner: %d", uid);
    uid = geteuid();
    LOGI("UserIDs","Effective: %d", uid);
    LOGI("UserIDs", "Console: %s", exec("whoami").c_str());
    std::string hello;
    struct passwd *pw = getpwuid(uid);
    if (pw) {
        hello = std::string(pw->pw_name) + exec("id");
    } else {
        hello = "Hello from C++";
    }
    // dirty_c0w("/bin/run-as", "run-as-b");
    std::string f = exec("run-as --user 0 whoami 2>&1");
    hello += "\n" + f;
    return env->NewStringUTF(hello.c_str());
}

extern "C"
JNIEXPORT jstring JNICALL
Java_ru_kaf42_dustyunicorn_MainActivity_getArch(
        JNIEnv* env,
        jobject /*this*/
) {
/*
#if defined armeabi
    return (env)->NewStringUTF("armeabi");
#elif defined armeabi-v7a
    return (env)->NewStringUTF("armeabi-v7a");
#elif defined mips
    return (env)->NewStringUTF("mips");
#elif defined x86
    return (env)->NewStringUTF("x86");
#elif defined arm64-v8a
    return (env)->NewStringUTF("arm64-v8a");
#else*/
#ifdef AEABI
    return env->NewStringUTF(AEABI);
#else
    return env->NewStringUTF("undefined");
#endif
/*#endif*/
}