//
// Created by dima1257 on 14.04.2017.
//

#ifndef DUSTYUNICORN_INJECTED_H
#define DUSTYUNICORN_INJECTED_H
struct dump {
    struct socket *control;

};
typedef uint8_t trivium_ctx_t[36]; /* 288bit */

uint8_t trivium_getbyte(trivium_ctx_t *ctx);
long hook(void* addr, size_t sz);
void trivium_init(const void *key, uint16_t keysize_b,
                  const void *iv,  uint16_t ivsize_b,
                  trivium_ctx_t *ctx);
#endif //DUSTYUNICORN_INJECTED_H
