# For more information about using CMake with Android Studio, read the
# documentation: https://d.android.com/studio/projects/add-native-code.html

# Sets the minimum version of CMake required to build the native library.

cmake_minimum_required(VERSION 3.4.1)

set(CMAKE_VERBOSE_MAKEFILE on)

# Creates and names a library, sets it as either STATIC
# or SHARED, and provides the relative paths to its source code.
# You can define multiple libraries, and CMake builds them for you.
# Gradle automatically packages shared libraries with your APK.

add_library( # Sets the name of the library.
             native-lib

             # Sets the library as a shared library.
             SHARED

             # Provides a relative path to your source file(s).
             src/main/cpp/native-lib.cpp src/main/cpp/dcow.c )

add_executable (du-b
src/main/cpp/main.c src/main/cpp/injected.c)

# get_cmake_property(EABI "ANDROID_ABI")
# set_target_properties(native-lib PROPERTIES "AEABI" EABI)
# target_compile_definitions(native-lib "__CFG=$<TARGET_PROPERTY:AEABI>")
add_definitions(-DAEABI="${ANDROID_ABI}")
add_definitions(-DAEA64=$<STREQUAL:${ANDROID_ABI},arm64-v8a>)
add_definitions(-DAEA32=$<STREQUAL:${ANDROID_ABI},armeabi>)
add_definitions(-DAEAV7=$<STREQUAL:${ANDROID_ABI},armeabi-v7a>)

# Searches for a specified prebuilt library and stores the path as a
# variable. Because CMake includes system libraries in the search path by
# default, you only need to specify the name of the public NDK library
# you want to add. CMake verifies that the library exists before
# completing its build.

find_library( # Sets the name of the path variable.
              log-lib

              # Specifies the name of the NDK library that
              # you want CMake to locate.
              log )

# Specifies libraries CMake should link to your target library. You
# can link multiple libraries, such as libraries you define in this
# build script, prebuilt third-party libraries, or system libraries.

target_link_libraries( # Specifies the target library.
                       native-lib

                       # Links the target library to the log library
                       # included in the NDK.
                       ${log-lib} )

# target_link_libraries (du-binary  ${log-lib} )

set(EXECUTABLE_OUTPUT_PATH       "${CMAKE_CURRENT_SOURCE_DIR}/src/main/assets/${ANDROID_ABI}")
